package builder

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/hashicorp/go-uuid"
	"github.com/hashicorp/packer/builder/amazon/common"
	"github.com/hashicorp/packer/packer"
	"github.com/hashicorp/packer/template/interpolate"
	"github.com/mitchellh/multistep"
	"reflect"
	"strings"
	"time"
)

const tagSleep = 5 * time.Second
const ownershipTagName = "Packer-Ownership"

type StepRunStoppedInstance struct {
	AmiId                             string `json:"image-id"`
	AssociatePublicIpAddress          bool
	AvailabilityZone                  string `json:"availability-zone"`
	BlockDevices                      common.BlockDevices
	Ctx                               interpolate.Context
	EbsOptimized                      bool
	InstanceType                      string `json:"instance-type"`
	Region                            string
	IamInstanceProfile                string
	IamInstanceProfileArn             string `json:"iam-instance-profile.arn"`
	InstanceInitiatedShutdownBehavior string
	KeypairName                       string            `json:"key-name"`
	SecurityGroups                    []string          `json:"instance.group-id"`
	SubnetId                          string            `json:"subnet-id"`
	RunTags                           map[string]string `json:"tag"`
	VolumeTags                        map[string]string
	UserData                          string
	UserDataFile                      string
	InstancePoolMinSize               int64
	ec2conn                           *ec2.EC2
	instanceId                        string
}

func (s *StepRunStoppedInstance) Run(state multistep.StateBag) multistep.StepAction {
	ui := state.Get("ui").(packer.Ui)
	s.ec2conn = state.Get("ec2").(*ec2.EC2)

	if s.KeypairName == "" {
		if name, ok := state.GetOk("keyPair"); ok {
			s.KeypairName = name.(string)
		}
	}

	if s.SecurityGroups == nil {
		s.SecurityGroups = state.Get("securityGroupIds").([]string)
	}

	if s.AmiId == "" {
		image, ok := state.Get("source_image").(*ec2.Image)
		if !ok {
			state.Put("error", fmt.Errorf("source_image type assertion failed"))
			return multistep.ActionHalt
		}
		s.AmiId = *image.ImageId
	}

	descInstanceInput, err := s.buildDescribeInstancesInput()
	if err != nil {
		state.Put("error", fmt.Errorf("Failed to build DescribeInstancesInput: %s", err.Error()))
		ui.Error(err.Error())
		return multistep.ActionHalt
	}

	ownershipUuid, err := uuid.GenerateUUID()
	if err != nil {
		err := fmt.Errorf("Failed to generate ownership UUID: %s", err)
		state.Put("error", err)
		ui.Error(err.Error())
		return multistep.ActionHalt
	}

	ui.Say(fmt.Sprintf("Using ownership UUID %s", ownershipUuid))

	owernshipTag, err := common.ConvertToEC2Tags(map[string]string{ownershipTagName: ownershipUuid}, *s.ec2conn.Config.Region, s.AmiId, s.Ctx)
	if err != nil {
		err := fmt.Errorf("Error creating ownership tag: %s", err)
		state.Put("error", err)
		ui.Error(err.Error())
		return multistep.ActionHalt
	}

	ui.Say("Searching for instances using the following criteria:")
	ui.Say(fmt.Sprintf("%+v", descInstanceInput))

	instanceOwned := false
	var instance *ec2.Instance

	for !instanceOwned {
		instance, err = s.getInstance(descInstanceInput)
		if err != nil {
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}

		ui.Say(fmt.Sprintf("Found instance with ID %s", *instance.InstanceId))

		createOwnershipTagInput := &ec2.CreateTagsInput{
			Resources: []*string{instance.InstanceId},
			Tags:      owernshipTag,
		}

		_, err = s.ec2conn.CreateTags(createOwnershipTagInput)
		if err != nil {
			err := fmt.Errorf("Error tagging source instance with ownership tag: %s", err)
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}

		time.Sleep(tagSleep)

		// check that our token is still there
		chkInstance, err := s.getInstance(&ec2.DescribeInstancesInput{
			InstanceIds: []*string{instance.InstanceId},
		})
		if err != nil {
			err := fmt.Errorf("Error when describing instance to check the ownership tag: %s", err.Error())
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}

		if !s.instanceWasTaggedWithOwnership(chkInstance, ownershipUuid) {
			ui.Say("Did not tag instance for ownership. Someone else may have claimed the instance. Trying again.")
		} else {
			ui.Say("Tagged instance with ownership UUID")
			instanceOwned = true
		}
	}

	startInput := &ec2.StartInstancesInput{
		InstanceIds: []*string{instance.InstanceId},
	}
	// the StartInstancesOutput returned below is just a list of what
	// instances have a state change. If we end up needing to check it
	// beyond seeing if there's an error, do so here.
	_, err = s.ec2conn.StartInstances(startInput)
	if err != nil {
		state.Put("error", fmt.Errorf("Starting the instance failed: %s", err.Error()))
		ui.Error(err.Error())
		return multistep.ActionHalt
	}

	s.instanceId = *instance.InstanceId
	state.Put("instance", instance)

	return multistep.ActionContinue
}

func (s *StepRunStoppedInstance) Cleanup(state multistep.StateBag) {
	ui := state.Get("ui").(packer.Ui)

	if s.instanceId != "" {
		err := s.terminateInstance(ui)

		if err != nil {
			ui.Error("Error when terminating instance. Not launching new instance into pool.")
			return
		}
	}

	launchedInstanceIds, err := s.launchNewInstanceIntoPool(ui)

	if err != nil {
		merr := fmt.Errorf("Error launching new instances into pool: %s", err)
		ui.Error(merr.Error())
		return
	}

	for _, id := range launchedInstanceIds {
		ui.Say(fmt.Sprintf("Launched instance id %s into pool", id))
	}

	return
}

func (s *StepRunStoppedInstance) createFilter(filterName string, filterValues interface{}) (*ec2.Filter, error) {
	var fVals []string
	switch filterValues := filterValues.(type) {
	case string:
		fVals = []string{filterValues}
	case []string:
		fVals = filterValues
	default:
		err := fmt.Errorf("tried to pass in %T to create a filter for %s! Object: %+v", filterName, filterValues, filterValues)
		return nil, err
	}
	filter := &ec2.Filter{
		Name:   aws.String(filterName),
		Values: aws.StringSlice(fVals),
	}
	return filter, nil
}

func (s *StepRunStoppedInstance) createFilterFromMap(filterName string, filterValues map[string]string) ([]*ec2.Filter, error) {
	filters := make([]*ec2.Filter, len(filterValues))
	i := 0
	for k, v := range filterValues {
		tag := strings.Join([]string{filterName, k}, ":")
		fstep, err := s.createFilter(tag, v)
		if err != nil {
			return nil, err
		}
		filters[i] = fstep
		i++
	}
	return filters, nil
}

func (s *StepRunStoppedInstance) buildDescribeInstancesInput() (*ec2.DescribeInstancesInput, error) {
	dii := new(ec2.DescribeInstancesInput)
	// run through the elements real quick to figure out how many elements
	// are present
	var argLen int
	g := reflect.ValueOf(s).Elem()
	for i := 0; i < g.NumField(); i++ {
		if g.Type().Field(i).Tag.Get("json") == "" {
			// skip if no json tag
			continue
		}
		switch g.Field(i).Kind() {
		case reflect.String:
			if g.Field(i).String() != "" {
				argLen++
			}
		case reflect.Slice:
			if !g.Field(i).IsNil() && g.Field(i).Len() != 0 {
				argLen++
			}
		case reflect.Map:
			if !g.Field(i).IsNil() && g.Field(i).Len() != 0 {
				argLen += g.Field(i).Len()
			}
		}
	}
	filters := make([]*ec2.Filter, 0, argLen)
	for i := 0; i < g.NumField(); i++ {
		field := g.Type().Field(i).Tag.Get("json")
		if field == "" {
			// skip if no json tag
			continue
		}
		val := g.Field(i).Interface()

		switch g.Field(i).Kind() {
		case reflect.String, reflect.Slice:
			f, err := s.createFilter(field, val)
			if err != nil {
				return nil, err
			}
			filters = append(filters, f)
		case reflect.Map:
			f, err := s.createFilterFromMap(field, val.(map[string]string))
			if err != nil {
				return nil, err
			}
			filters = append(filters, f...)
		}
	}

	stoppedFilter, err := s.createFilter("instance-state-name", "stopped")
	if err != nil {
		return nil, err
	}

	unownedTagFilter, err := s.createFilter(fmt.Sprintf("tag:%s", ownershipTagName), "none")
	if err != nil {
		return nil, err
	}

	filters = append(filters, stoppedFilter, unownedTagFilter)

	dii.Filters = filters
	return dii, nil
}

func (s *StepRunStoppedInstance) getInstance(descInstanceInput *ec2.DescribeInstancesInput) (*ec2.Instance, error) {
	descInstancesOutput, err := s.ec2conn.DescribeInstances(descInstanceInput)
	if err != nil {
		merr := fmt.Errorf("Failed to describe instances: %s", err.Error())
		return nil, merr
	}

	if len(descInstancesOutput.Reservations) == 0 || len(descInstancesOutput.Reservations[0].Instances) == 0 {
		merr := fmt.Errorf("No instances matching pool requirements were found")
		return nil, merr
	}

	return descInstancesOutput.Reservations[0].Instances[0], nil
}

func (s *StepRunStoppedInstance) extractUserData() (string, error) {
	// Copying from packer's StepRunSourceInstance
	userData := s.UserData

	if s.UserDataFile != "" {

		contents, err := ioutil.ReadFile(s.UserDataFile)
		if err != nil {
			err = fmt.Errorf("Problem reading user data file: %s", err.Error())
			return "", err
		}

		userData = string(contents)
	}
	// Test if it is encoded already, and if not, encode it
	if _, err := base64.StdEncoding.DecodeString(userData); err != nil {
		log.Printf("[DEBUG] base64 encoding user data...")
		userData = base64.StdEncoding.EncodeToString([]byte(userData))
	}

	return userData, nil
}

func (s *StepRunStoppedInstance) terminateInstance(ui packer.Ui) error {
	ui.Say("Terminating the AWS instance...")
	if _, err := s.ec2conn.TerminateInstances(&ec2.TerminateInstancesInput{InstanceIds: []*string{&s.instanceId}}); err != nil {
		ui.Error(fmt.Sprintf("Error terminating instance, may still be around: %s", err))
		return err
	}
	stateChange := common.StateChangeConf{
		Pending: []string{"pending", "running", "shutting-down", "stopped", "stopping"},
		Refresh: common.InstanceStateRefreshFunc(s.ec2conn, s.instanceId),
		Target:  "terminated",
	}

	_, err := common.WaitForState(&stateChange)
	if err != nil {
		ui.Error(err.Error())
		return err
	}

	return nil
}

func (s *StepRunStoppedInstance) determineNumInstancesToLaunch() (int64, error) {
	launchInstanceCount := s.InstancePoolMinSize

	descInstancesInput, err := s.buildDescribeInstancesInput()
	if err != nil {
		return 1, err
	}

	descInstancesOutput, err := s.ec2conn.DescribeInstances(descInstancesInput)
	if err != nil {
		return 1, err
	}
	for _, r := range descInstancesOutput.Reservations {
		launchInstanceCount -= int64(len(r.Instances))
	}

	return launchInstanceCount, nil
}

func (s *StepRunStoppedInstance) launchNewInstanceIntoPool(ui packer.Ui) ([]string, error) {
	userData, err := s.extractUserData()
	if err != nil {
		return make([]string, 0), err
	}

	ec2Tags, err := common.ConvertToEC2Tags(s.RunTags, *s.ec2conn.Config.Region, s.AmiId, s.Ctx)
	if err != nil {
		err := fmt.Errorf("Error tagging source instance: %s", err)
		ui.Error(err.Error())
		return make([]string, 0), err
	}

	ec2Tags = append(ec2Tags, &ec2.Tag{
		Key:   aws.String(ownershipTagName),
		Value: aws.String("none"),
	})

	common.ReportTags(ui, ec2Tags)

	volTags, err := common.ConvertToEC2Tags(s.VolumeTags, *s.ec2conn.Config.Region, s.AmiId, s.Ctx)
	if err != nil {
		err := fmt.Errorf("Error tagging volumes: %s", err)
		ui.Error(err.Error())
		return make([]string, 0), err
	}

	instancesToLaunch, err := s.determineNumInstancesToLaunch()

	if err != nil {
		err := fmt.Errorf("Could not describe current instance pool count. Will start 1 new instance into the pool: %s", err)
		ui.Error(err.Error())
	}

	if instancesToLaunch <= 0 {
		ui.Say("Pool is already at minimum size. Not launching any more instances.")
		return make([]string, 0), nil
	}

	runOpts := &ec2.RunInstancesInput{
		ImageId:                           &s.AmiId,
		InstanceType:                      &s.InstanceType,
		UserData:                          &userData,
		MaxCount:                          aws.Int64(instancesToLaunch),
		MinCount:                          aws.Int64(instancesToLaunch),
		IamInstanceProfile:                &ec2.IamInstanceProfileSpecification{Name: &s.IamInstanceProfile},
		BlockDeviceMappings:               s.BlockDevices.BuildLaunchDevices(),
		Placement:                         &ec2.Placement{AvailabilityZone: &s.AvailabilityZone},
		EbsOptimized:                      &s.EbsOptimized,
		InstanceInitiatedShutdownBehavior: &s.InstanceInitiatedShutdownBehavior,
	}

	var tagSpecs []*ec2.TagSpecification

	if len(ec2Tags) > 0 {
		runTags := &ec2.TagSpecification{
			ResourceType: aws.String("instance"),
			Tags:         ec2Tags,
		}

		tagSpecs = append(tagSpecs, runTags)
	}

	if len(volTags) > 0 {
		runVolTags := &ec2.TagSpecification{
			ResourceType: aws.String("volume"),
			Tags:         volTags,
		}

		tagSpecs = append(tagSpecs, runVolTags)
	}

	if len(tagSpecs) > 0 {
		runOpts.SetTagSpecifications(tagSpecs)
	}

	if s.KeypairName != "" {
		runOpts.KeyName = &s.KeypairName
	}

	if s.SubnetId != "" && s.AssociatePublicIpAddress {
		runOpts.NetworkInterfaces = []*ec2.InstanceNetworkInterfaceSpecification{
			{
				DeviceIndex:              aws.Int64(0),
				AssociatePublicIpAddress: &s.AssociatePublicIpAddress,
				SubnetId:                 &s.SubnetId,
				Groups:                   aws.StringSlice(s.SecurityGroups),
				DeleteOnTermination:      aws.Bool(true),
			},
		}
	} else {
		runOpts.SubnetId = &s.SubnetId
		runOpts.SecurityGroupIds = aws.StringSlice(s.SecurityGroups)
	}

	runResp, err := s.ec2conn.RunInstances(runOpts)
	if err != nil {
		err := fmt.Errorf("Error launching source instance: %s", err)
		ui.Error(err.Error())
		return make([]string, 0), err
	}

	instanceIds := []string{}
	for _, instance := range runResp.Instances {
		instanceIds = append(instanceIds, *instance.InstanceId)
	}

	return instanceIds, nil
}

func (s *StepRunStoppedInstance) instanceWasTaggedWithOwnership(instance *ec2.Instance, ownershipUuid string) bool {
	for _, tag := range instance.Tags {
		if *tag.Key == ownershipTagName && *tag.Value == ownershipUuid {
			return true
		}
	}

	return false
}
