package builder

import (
	"fmt"
	amazonEbsBuilder "github.com/hashicorp/packer/builder/amazon/ebs"
	"github.com/hashicorp/packer/packer"
	"github.com/hashicorp/packer/template/interpolate"
	"strings"
)

type StoppedInstancePoolBuilderConfig struct {
	amazonEbsBuilder.Config `mapstructure:",squash"`
	InstancePoolMinSize     int64  `mapstructure:"instance_pool_min_size"`
	IamInstanceProfileArn   string `mapstructure:"iam_instance_profile_arn"`

	ctx interpolate.Context
}

func (c *StoppedInstancePoolBuilderConfig) Prepare(ctx *interpolate.Context) []error {
	var errs *packer.MultiError

	if c.InstancePoolMinSize <= 0 {
		errs = packer.MultiErrorAppend(errs, fmt.Errorf("Stopped instance pool min size must be greater than 0"))
	}

	if c.SSHKeyPairName == "" || c.RunConfig.Comm.SSHPrivateKey == "" {
		errs = packer.MultiErrorAppend(errs, fmt.Errorf("SSH Key Pair name and private key file must be supplied"))
	}

	if c.IamInstanceProfile != "" {
		errs = packer.MultiErrorAppend(errs, fmt.Errorf("IamInstanceProfileArn must be specified in place of IamInstanceProfile"))
	}

	errs = packer.MultiErrorAppend(errs, c.Config.AccessConfig.Prepare(&c.ctx)...)
	errs = packer.MultiErrorAppend(errs,
		c.Config.AMIConfig.Prepare(&c.Config.AccessConfig, &c.ctx)...)
	errs = packer.MultiErrorAppend(errs, c.Config.BlockDevices.Prepare(&c.ctx)...)
	errs = packer.MultiErrorAppend(errs, c.Config.RunConfig.Prepare(&c.ctx)...)

	if c.IamInstanceProfileArn != "" {
		lastSlashIndex := strings.LastIndex(c.IamInstanceProfileArn, "/")
		c.IamInstanceProfile = c.IamInstanceProfileArn[lastSlashIndex+1 : len(c.IamInstanceProfileArn)]
	}

	return errs.Errors
}
