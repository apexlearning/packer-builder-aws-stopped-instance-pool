package main

import (
	"bitbucket.org/apexlearning/packer-builder-aws-stopped-instance-pool/builder"
	"github.com/hashicorp/packer/packer/plugin"
)

func main() {
	server, err := plugin.Server()
	if err != nil {
		panic(err)
	}
	server.RegisterBuilder(new(builder.StoppedInstancePoolBuilder))
	server.Serve()
}
