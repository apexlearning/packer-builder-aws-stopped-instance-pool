GOPATHDIR = gopath
PROJNAME = bitbucket.org/apexlearning/packer-builder-aws-stopped-instance-pool
GOPROJSRCPATH = $(GOPATHDIR)/src/$(PROJNAME)

export GOPATH := $(abspath $(GOPATHDIR))

.PHONY: copy_src_to_gopath fmt vendor test build install clean clean-vendor clean-go

copy_src_to_gopath:
	mkdir -p "$(GOPROJSRCPATH)"
	find . -maxdepth 1 \
		-not -name ".*" \
		-not -name "$(GOPATHDIR)" \
		-not -name "Makefile" \
		-not -name ".gitignore" \
		-exec cp -R {} "$(GOPROJSRCPATH)" \;

fmt:
	find . \
		-not \( -path "./$(GOPATHDIR)" -prune \) \
		-not \( -path "./vendor" -prune \) \
		-name "*.go" \
		-exec go fmt {} \;

vendor: copy_src_to_gopath
	cd "$(GOPROJSRCPATH)" && govendor sync

test: clean fmt vendor
	cd "$(GOPROJSRCPATH)" && go test ./...

build: clean fmt vendor
	go build $(PROJNAME)

install: clean fmt vendor
	go install $(PROJNAME)

clean:
	[[ -d "$(GOPROJSRCPATH)" ]] && \
	find "$(GOPROJSRCPATH)" -maxdepth 1 \
		-not -path "$(GOPROJSRCPATH)" \
		-not -name ".*" \
		-not -name "vendor" \
		-exec rm -rf {} \; || \
	true

clean-vendor:
	rm -rf "$(GOPROJSRCPATH)/vendor"

clean-go:
	rm -rf $(GOPATHDIR)/{bin,pkg,src}
