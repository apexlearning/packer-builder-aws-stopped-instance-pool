# AWS Stopped Instance Pool Packer Plugin

A builder for packer which will create an AWS AMI using an existing EC2 instance
from a pool of stopped instances as its starting point instead of starting a new
EC2 instance from scratch. This will speed up the packing process, especially in
the case of Windows where there is an unavoidable wait time when starting a
new instance.

## Development

### Prerequisites

* Go 1.x installed

### Building

#### Standard Go Toolchain

You can simply add the project to your `GOPATH` through the usual

    go get -d bitbucket.org/apexlearning/packer-builder-aws-stopped-instance-pool

This project uses [govendor][1] for dependency management, so after `go get`ing
this package it is recommended that you fetch dependencies by running the
following command from the project root

    govendor sync

Then use the go toolchain as normal.

#### Project-Isolated Toolchain

You can also build and develop the project isolated to this repository without
adding it to your normal `GOPATH` and related toolchain.

In addition to the normal pre-requisites, you'll also need:

* `make` installed
* common shell utilities such as `find`, `rm`, and so on available on your `PATH`

The `Makefile` will manage its own `GOPATH` here in the project root for local
development.

Simply replace `go` with `make` to handle most normal go development tasks.

```
make fmt # to format the project's go files
```

```
make vendor # to fetch dependencies into the project's GOPATH
```

```
make test # to execute the project's tests
```

```
make build # to build the project using the project's GOPATH
```

```
make install # to install the project into the project's GOPATH
```

```
make clean # to remove _only the project's_ source files from the project's GOPATH
```

```
make clean-go # to remove the entire project GOPATH
```

### GoLand Specifics

If you're using [JetBrains GoLand][2], there are run configurations checked into the repository.
When you initialize the project for the first time, it's possible that GoLand will delete the `.idea` directory.
If that happens, just perform a `git checkout` on the `.idea` directory to restore the run configurations.

## License Information

Copyright (C) 2018 Apex Learning Inc.

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at <http://mozilla.org/MPL/2.0/>.

Some Source Code has also been copied from the Hashicorp packer project, which
is also available under the Mozilla Public License, v. 2.0 and can be retrieved
from <https://github.com/hashicorp/packer>

See [LICENSE](LICENSE) for more details.

[1]: https://github.com/kardianos/govendor
[2]: https://www.jetbrains.com/go/